#!/bin/bash
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

apt-get install -y lsb-release wget

wget -O - https://bitbucket.org/kai-innovations/scripts/raw/master/getSalt.sh | apt-key add -

os=`lsb_release -si`
ver=`lsb_release -sr`

if [[ "$os" == "Ubuntu" && "$ver" == 14.* ]]; then
  echo "deb [arch=all] https://cobalt.azu.oscar-emr.net:63827/repository/salt-ovh-xenial/ xenial main" > /etc/apt/sources.list.d/saltstack.list
elif [[ "$os" == "Ubuntu" && "$ver" == 16.* ]]; then
  echo "deb [arch=all] https://cobalt.azu.oscar-emr.net:63827/repository/salt-ovh-xenial/ xenial main" > /etc/apt/sources.list.d/saltstack.list
elif [[ "$os" == "Ubuntu" && "$ver" == 18.* ]]; then
  echo "deb [arch=all] https://cobalt.azu.oscar-emr.net:63827/repository/salt-ovh-focal/ focal main" > /etc/apt/sources.list.d/saltstack.list
elif [[ "$os" == "Ubuntu" && "$ver" == 20.* ]]; then
  echo "deb [arch=all] https://cobalt.azu.oscar-emr.net:63827/repository/salt-ovh-focal/ focal main" > /etc/apt/sources.list.d/saltstack.list
else
  echo "This version of linux is not currently supported"
  exit 0
fi

echo "What is the salt ID?"
read saltID
mkdir -p /etc/salt
echo $saltID > /etc/salt/minion_id

mkdir -p /etc/salt/minion.d
echo "master: cobalt.azu.oscar-emr.net" > /etc/salt/minion.d/master.conf


apt-get update
apt-get install -y salt-minion

service salt-minion restart

