#!/bin/bash
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

sudo rm /opt/vpn/*
if [ "$1" = "" ]
then
 echo "What is the salt ID?"
 read saltID
else
 saltID=$1
fi

echo $saltID > /etc/salt/minion_id
echo $saltID > /etc/hostname
sed -i "s/C1000000001-S1/$saltID/g" /etc/hosts
hostnamectl set-hostname $saltID

service salt-minion restart

echo "Please access the salt id on the salt master and then press ENTER"
echo "salt-key -a $saltID"
read keyAccepted

salt-call state.apply Configurations.Salt.Minion test=False -l info
service salt-minion restart
salt-call state.apply test=False -l info
service openvpn restart
echo "============================================================================================================"
echo "Script is complete. If anything gave an error, please review and correct before continuing with system setup."

read -r -p "Reboot to now occur? [Y/n] " response
if [[ "$response" =~ ^([Nn][Oo]|[Nn])$ ]]
then
 echo "Aborting Reboot, please remember to Reboot Server prior to Production"
else
 reboot -h now
fi