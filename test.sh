#!/bin/bash

path="$certificatePath"
excluded_certs=(${excludedCertificates[@]})
threshold_days=$expirationThresholdDays

# Retrieve certificates from the specified path
certificates=($(find "$path" -type f -name "*.pem"))

# Iterate through each certificate and check its expiration date
for cert in "${certificates[@]}"; do
  # Check if the certificate should be excluded based on its file name
  if [[ "${excluded_certs[*]}" =~ $(basename "$cert") ]]; then
    continue  # Skip this certificate
  fi

  expiration_date=$(openssl x509 -enddate -noout -in "$cert" | awk -F "=" '{print $2}')
  expiry_timestamp=$(date -d "$expiration_date" +%s)
  current_timestamp=$(date +%s)

  # Calculate the remaining days until expiration
  remaining_days=$(( ($expiry_timestamp - $current_timestamp) / 86400 ))

  # Print details of certificates that are expiring soon or have already expired
  if [[ $remaining_days -lt 0 ]]; then
    echo "Certificate $cert has already expired."
  elif [[ $remaining_days -lt $threshold_days ]]; then
    echo "Certificate $cert will expire in $remaining_days days."
  fi
done
